+++
title = 'Februari'
date = 2024-03-06T14:05:00+08:00
publishdate = 2024-03-06T14:05:00+08:00
description = "Single Sign-On Project"
icon = "rocket_launch"
author = "Erol Tooy"
draft = false
toc = true
weight = 200
tags = ["Beginners"]
+++

## Research SSO

{{< table "table-responsive" >}}
| Tanggal | Deskripsi |
|---|---|
| 5-7 Februari | Membuat list contoh SSO software untuk dipelajari flow-nya. Diperoleh 2 software yang dinilai aman yaitu Keycloak dan Kratos by Ory. Parameternya dilihat dari banyaknya kontributor dan pembaruan terbaru dari sebuah software. |
{{< /table >}}

---

## Deploy dan testing OSS software Keycloak

{{< table "table-responsive" >}}
| Tanggal | Deskripsi |
|---|---|
| 8-12 Februari | Instalasi dan running docker package di localhost. |
{{< /table >}}

## Deploy dan konfigurasi VPS untuk OSS software Kratos by Ory

{{< table "table-responsive" >}}
| Tanggal | Deskripsi |
|---|---|
| 12-19 Februari | (1) Setup dan konfigurasi VPS, termasuk instalasi nginx dan postgresql. (2) Deploy Kratos di VPS. |
{{< /table >}}

## Testing OSS software Kratos by Ory

{{< table "table-responsive" >}}
| Tanggal | Deskripsi |
|---|---|
| 20-29 Februari | Sukses memahami flow dari software Kratos by Ory. |
| | Demo URL: https://kratos.erto.work/auth/login |

{{< /table >}}
